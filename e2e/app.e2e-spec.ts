import { FoodappPage } from './app.po';

describe('foodapp App', function() {
  let page: FoodappPage;

  beforeEach(() => {
    page = new FoodappPage();
  });

  it('should display message saying app works', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('app works!');
  });
});
